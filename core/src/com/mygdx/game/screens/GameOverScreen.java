package com.mygdx.game.screens;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Texture;
import com.mygdx.game.Constants;
import com.mygdx.game.Joc;



public class GameOverScreen extends ScreenAdapter{
	
	//Si posem final ens assegurem que sera de nomes lectura 
		final Joc joc;

		Texture gameover; //Imatge a mostrar
		float tempsAcumulat; //Temps que porta la SplashScreen activa

		public GameOverScreen(Joc joc) {
			this.joc = joc;
			gameover = new Texture("gameover.jpg");
			tempsAcumulat = 0;
		}

		@Override
		public void render(float delta) {
			super.render(delta);

			tempsAcumulat += delta;

			if (tempsAcumulat > Constants.TEMPS_ENTRE_GAMEOVER_SPLASH_SCREEN) {
				joc.setScreen(new SplashScreen(joc));
				this.dispose();
			} else {	//Amb aquest else evitem dibuixar coses que s'han destruit al dispose
				int width = Gdx.graphics.getWidth();
				int height = Gdx.graphics.getHeight();

				joc.batch.begin();
				joc.batch.draw(gameover, 0, 0, width, height);
				joc.batch.end();
			}

		}

		@Override
		public void dispose() {
			super.dispose();
			gameover.dispose();
		}

}