package com.mygdx.game.screens;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.game.Constants;
import com.mygdx.game.Joc;

public class GameScreen extends ScreenAdapter {

	// Part grafica del videojoc
	Texture personatgeTex;
	Texture enemicTex;
	Texture disparPlayerTex;

	// Model de dades
	// Tant el jugador com els enemics son Rectangle
	Rectangle personatge;
	List<Rectangle> enemics;
	float tempsAcumulat;
	List<Rectangle> disparsPlayer;
	
  	
	//Sempre tenim una referencia a Joc
	final Joc joc;

	public GameScreen(Joc joc) {
		
		this.joc=joc;
		
		personatge = new Rectangle(Constants.PLAYER_POSX_INICIAL, Constants.PLAYER_POSY_INICIAL, Constants.PLAYER_AMPLE,
				Constants.PLAYER_ALT);

		personatgeTex = new Texture("playerShip2_green.png");
		enemicTex = new Texture("ufoRed.png");
		disparPlayerTex = new Texture("ufoRed.png");

		// Inicialment no hi ha obstacles
		enemics = new ArrayList<Rectangle>();
		tempsAcumulat = 0;
		
		//	Inicialment no hi ha dispars
		disparsPlayer= new ArrayList<Rectangle>();
		 
	}

	@Override
	public void render(float delta) {  //El render de Screen ja rep un delta
		super.render(delta);
		// Sempre farem el seguent:
		// Input de l'usuari
		// Moure els elements de videojoc: enemics, dispars, colisions...
		// Dibuixar

		input(delta);

		update(delta);

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		//Ara el SpriteBatch es troba en la classe Joc
		joc.batch.begin();
		joc.batch.draw(personatgeTex, personatge.x, personatge.y, personatge.width, personatge.height);
		for (Rectangle obstacle : enemics) {
			joc.batch.draw(enemicTex, obstacle.x, obstacle.y, obstacle.width, obstacle.height);
		}
		for (Rectangle dispar : disparsPlayer) {
			joc.batch.draw(disparPlayerTex, dispar.x, dispar.y, dispar.width, dispar.height);
		} 
		joc.batch.end();
	}
	
	private void update(float delta) {

		tempsAcumulat += delta;

		// Nou obstacle
		if (tempsAcumulat > Constants.TEMPS_NOU_ENEMIC) {
			tempsAcumulat = 0;// RESET. Important!!!
			
			//Generem aleatoriament la posicio dels obstacles
			//No agafem tot l'ample de pantalla. Deixem un poc de 
			//marge per a que l'obstacle no estigui mig fora
			int amplePantalla=Gdx.graphics.getWidth();
			int posX=(int) ((amplePantalla-30.0f)*Math.random());		
			enemics.add(new Rectangle(posX, 500, 30, 30));
		}

		// Moure obstacles
		for (Rectangle obstacle : enemics) {
			obstacle.y = obstacle.y - Constants.ENEMIC_VELOCITATY*delta;;
		}

		// Eliminar obstacles
		for (int i = 0; i < enemics.size(); i++) {
			Rectangle obstacle = enemics.get(i);

				
			//Fora pantalla
			// Decidim eliminar els obstacles que toquen la part inferior
			if (obstacle.y < 0) {
				enemics.remove(i);
			}
			
			//Colisions amb el player
			if(obstacle.overlaps(personatge)) {
				joc.setScreen(new GameOverScreen(joc));
			}

		}
		
		//Moure dispars
		for (Rectangle dispar : disparsPlayer) {
			dispar.y += Constants.DISPAR_PLAYER_VELOCITATY * delta;
		}
				
		//Eliminem dispars
		for (int i = 0; i < disparsPlayer.size(); i++) {
			Rectangle dispar = disparsPlayer.get(i);

			//Sortit pantalla
			//if(dispar.y > Gdx.graphics.getHeight()) {
			if(dispar.y > 300) {
				disparsPlayer.remove(i);

				//Com el dispar surt de la pantalla ja no 
				//comprovem els xocs amb els enemics
				break;
			}
		 
			//Xocs enemic
			//Hem de comparar cada dispar amb tots els enemics
			for (int j = 0; j < enemics.size(); j++) {
				Rectangle enemic = enemics.get(j);
				
				if(enemic.overlaps(dispar)) {
					enemics.remove(j);
					disparsPlayer.remove(i);
				}
			}
			
			
						
			
		}
		



		
		
		
	}

	private void input(float delta) {
				 
		if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
			personatge.x+=Constants.PLAYER_VELOCITATX*delta;			
		}
		if (Gdx.input.isKeyPressed(Keys.LEFT)) {
			personatge.x-=Constants.PLAYER_VELOCITATX*delta;
		}
		if (Gdx.input.isKeyPressed(Keys.UP)) {
			personatge.y+=Constants.PLAYER_VELOCITATY*delta;
		}
		if (Gdx.input.isKeyPressed(Keys.DOWN)) {
			personatge.y-=Constants.PLAYER_VELOCITATY*delta;
		}
		
		if (Gdx.input.isKeyJustPressed(Keys.SPACE)) {
			float x=personatge.x+(personatge.width /2)-(Constants.DISPAR_PLAYER_AMPLE/2);
			float y =personatge.y + personatge.height ;
			
			Rectangle nouDispar=new Rectangle(
					x, 
					y, 
					Constants.DISPAR_PLAYER_AMPLE, 
					Constants.DISPAR_PLAYER_ALT);
			disparsPlayer.add(nouDispar);
		}

	}
	
	@Override
	public void dispose() {
	
		super.dispose();
		personatgeTex.dispose();
		enemicTex.dispose();
	}
}
