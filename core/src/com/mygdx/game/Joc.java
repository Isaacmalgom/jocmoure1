package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.screens.GameScreen;
import com.mygdx.game.screens.SplashScreen;

public class Joc extends Game {
	
	public SpriteBatch batch;
		

	@Override
	public void create() {
		batch = new SpriteBatch();
		setScreen(new SplashScreen(this));
		
	}

	@Override
	public void render() {
		super.render();
		
	}

	

	@Override
	public void dispose() {
		batch.dispose();
		
	}
}
