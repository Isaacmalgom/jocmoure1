package com.mygdx.game;

public class Constants {
	
	public static final int PLAYER_AMPLE=22;
	public static final int PLAYER_ALT = 22;
	public static final int PLAYER_POSX_INICIAL = 0;
	public static final int PLAYER_POSY_INICIAL = 0;
	public static final float PLAYER_VELOCITATX = 100;
	public static final float PLAYER_VELOCITATY = 100;
	
	public static final float TEMPS_NOU_ENEMIC = 1;
	public static final float ENEMIC_VELOCITATY = 100;
	public static final float TEMPS_ENTRE_SPLASH_GAME_SCREEN = 2;
	public static final float TEMPS_ENTRE_GAMEOVER_SPLASH_SCREEN = 2;
	
	public static final float DISPAR_PLAYER_AMPLE = 5;
	public static final float DISPAR_PLAYER_ALT=10;
	public static final float DISPAR_PLAYER_VELOCITATY=80;
	
	

}
